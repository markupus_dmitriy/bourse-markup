'use strict';



$(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
        searching: false,
        paging: false,
        ordering: true,
        info: false,
        fixedHeader: false,
        scrollCollapse: false,
        fixedColumns: false,
        columnDefs: [
            { responsivePriority: 6 },
            { responsivePriority: 5 },
            { responsivePriority: 4 },
            { responsivePriority: 3 },
            { responsivePriority: 2 },
            { responsivePriority: 1 }
        ]
    });
} );


$(".popup-block .close").click(function(e) {
    e.preventDefault();
    $(".popup-block").addClass('remove');
})
